package de.fu_berlin.agdb.swiss_tax.controller;

import de.fu_berlin.agdb.swiss_tax.model.Canton;
import de.fu_berlin.agdb.swiss_tax.model.MaritalStatus;
import de.fu_berlin.agdb.swiss_tax.model.Tariff;
import org.junit.jupiter.api.Test;

import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class TaxControllerTest {

  static List<String> tariffs = new LinkedList<>();

  // provide some test data for the tests
  static {
    tariffs.add("0601BEA0Y       20200101000285100000005000 0000000000000518");
    tariffs.add("0601BEA0Y       20200101029700100000100000 0000000000003805");
    tariffs.add("0601BEA1Y       20200101000490100000005000 0100000000000753");
    tariffs.add("0601BEA1Y       20200101001665100000005000 0100000000002224");
    tariffs.add("0601BEA1Y       20200101026300100000100000 0100000000003785");
    tariffs.add("0601BEB0Y       20200101004555100000005000 0000000000003169");
    tariffs.add("0601BEB5Y       20200101001340100000005000 0500000000000824");
    tariffs.add("0601BEB9Y       20200101003435100000005000 0900000000002079");
    tariffs.add("0601BEC0Y       20200101000365100000005000 0000000000000834");
    tariffs.add("0601BEC0Y       20200101003675100000005000 0000000000003063");
    //                           ^         ^                             ^
    //                  date yyyymmdd    amount: 36.751,00        tax rate: 30,63
    tariffs.add("0601BEC0Y       20200101001090100000005000 0000000000001765");
    tariffs.add("0601BEA4Y       20200101001260100000005000 0400000000001267");
    tariffs.add("0601BEA4N       20200101001085100000005000 0400000000000997");
    tariffs.add("0601BEH8Y       20200101001555100000005000 0800000000000600");
    //                           ^         ^                             ^
    //                  date yyyymmdd    amount: 15.551,00    tax rate: 6.00
    tariffs.add("0601BEH8Y       20200101004245100000005000 0800000000002483");
  } //               ^  ^
  //        BE (canton)  H- Tariff 8 - number of children, Y = church taxes

  @Test
  void getTariff(){
    TaxController controller =  new TaxController();
    // for the unit tests take a small data set do not load all 300 million lines from the zip file.
    controller.setTariffData(tariffs);
    double taxRate = controller.calculateTaxRate(Tariff.A, 0,2851.00, Canton.BERN);
    // TODO according to the tax table, it should return 5,18 not 0.0
    assertEquals(0.0, taxRate);
  }

  @Test
  void getTariff_single() {
    TaxController controller = new TaxController();
    // given single with no children
    // when we make calculation
    Tariff actual = controller.getTariff(1, MaritalStatus.SINGLE, true);
    assertEquals(Tariff.H, actual);
  }

  @Test
  void getTariff_single_children() {
    TaxController controller = new TaxController();
    // given single with children
    // when we make calculation
    Tariff actual = controller.getTariff(0, MaritalStatus.SINGLE, true);
    assertEquals(Tariff.A, actual);

  }


  @Test
  void getTariff_married() {
    TaxController controller = new TaxController();
    // given married with no children
    // when we make calculation
    Tariff actual = controller.getTariff(0, MaritalStatus.MARRIED, true);
    assertEquals(Tariff.B, actual);
  }

  @Test
  void getTariff_civil_union() {
    TaxController controller = new TaxController();
    // given civil union with sole earner
    // when we make calculation
    Tariff actual = controller.getTariff(0, MaritalStatus.CIVIL_UNION, true);
    assertEquals(Tariff.B, actual);
  }

  @Test
  void getTariff_other() {
    TaxController controller = new TaxController();
    Tariff actual = controller.getTariff(0, MaritalStatus.WIDOWED, true);
    assertEquals(Tariff.A, actual);
    actual = controller.getTariff(1, MaritalStatus.SINGLE, true);
    assertEquals(Tariff.H, actual);
    actual = controller.getTariff(1, MaritalStatus.DIVORCED, true);
    assertEquals(Tariff.H, actual);
    actual = controller.getTariff(1, MaritalStatus.WIDOWED, true);
    assertEquals(Tariff.H, actual);
  }

  @Test
  void getTariff_civil_union_both() {
    TaxController controller = new TaxController();
    // given civil union with sole earner
    // when we make calculation
    Tariff actual = controller.getTariff(0, MaritalStatus.CIVIL_UNION, false);
    assertEquals(Tariff.C, actual);
  }


  @Test
  void getTariff_married_both() {
    TaxController controller = new TaxController();
    // given married with no children
    // when we make calculation
    Tariff actual = controller.getTariff(0, MaritalStatus.MARRIED, false);
    assertEquals(Tariff.C, actual);
  }

  @Test
  void calculateTaxRate() {
  }
}
