package de.fu_berlin.agdb.swiss_tax.controller;

import de.fu_berlin.agdb.swiss_tax.SwissTaxApplication;
import io.restassured.RestAssured;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static io.restassured.RestAssured.*;


/**
 * An integration test that starts all the spring web application.
 * We use rest-assured library to test our REST endpoints here.
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {SwissTaxApplication.class})
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class TaxControllerIT {

  @Value("${local.server.port}")
  private int serverPort;

  @BeforeEach
  public void setUp() {
    // configure our rest client library to call the right http port
    RestAssured.port = serverPort;
  }

  /**
   * Perform an integration test using rest-assured library.
   * For this test a http server with this spring boot server is started.
   * We check that the REST-endpoint "/taxrate" should return http code 200
   */
  @Test
  void getTaxes() {
    // TODO: the rest endpoint requires paramters: "maritalStatus", "children", "amount" and "canton".
    // add them to the request and check that HTTP 200 instead of HTTP 400 is returned
      when().get("/taxrate").then().statusCode(HttpStatus.SC_BAD_REQUEST);
  }
}
