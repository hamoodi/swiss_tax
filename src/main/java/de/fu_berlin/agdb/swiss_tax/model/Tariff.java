package de.fu_berlin.agdb.swiss_tax.model;

/**
 * Tariff codes used for tax calculation in Switzerland.
 */
public enum Tariff {
  A, // separated, widowed, or single without children
  B, // married or in civil union sole earner
  C, // married or in civil union both
  H, // separated, widowed, or single with children
}
