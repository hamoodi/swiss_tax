package de.fu_berlin.agdb.swiss_tax;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Unzip a file to local classpath
 */
@Service
public class UnzipService {

  private final static String RESOURCES = "file_resources";
  private final static Logger log = LoggerFactory.getLogger(UnzipService.class);

  @Autowired
  ResourceLoader resourceLoader;

  /**
   * Unzip resources that were included in src/main/resources
   *
   * @param filename path to a file resource
   * @throws IOException
   */
  public void unzip(String filename) throws IOException {
    byte[] buffer = new byte[1024];
    InputStream inputStream = resourceLoader.getResource(String.format("classpath:%s",filename)).getInputStream();
    ZipInputStream zis = new ZipInputStream(inputStream);
    ZipEntry zipEntry = zis.getNextEntry();
    Path resourceDir = Paths.get(RESOURCES);
    if(!resourceDir.getFileName().toFile().exists()) {
      Files.createDirectories(resourceDir);
    }
    while (zipEntry != null) {
      File newFile = newFile(resourceDir.getFileName().toFile(), zipEntry);
      FileOutputStream fos = new FileOutputStream(newFile);
      int len;
      while ((len = zis.read(buffer)) > 0) {
        fos.write(buffer, 0, len);
      }
      log.info("Unzipped {}", newFile);
      fos.close();
      zipEntry = zis.getNextEntry();
    }
    zis.closeEntry();
    zis.close();
  }

  public Path load(String filename) {
    return Paths.get(RESOURCES).resolve(filename);
  }

  private static File newFile(File destinationDir, ZipEntry zipEntry) throws IOException {
    File destFile = new File(destinationDir, zipEntry.getName());

    String destDirPath = destinationDir.getCanonicalPath();
    String destFilePath = destFile.getCanonicalPath();

    if (!destFilePath.startsWith(destDirPath + File.separator)) {
      throw new IOException("Entry is outside of the target dir: " + zipEntry.getName());
    }

    return destFile;
  }
}
