Create TABLE Canton
(
  code char(2) not null,
  name varchar(255) not null,
  primary key (code)
);

Create TABLE MaritalStatus
(
  code char(1) not null,
  name varchar(255) not null,
  primary key (code)
);

INSERT INTO MaritalStatus(name, code) VALUES
  ('civil union', 'C'),
  ('divorced', 'D'),
  ('single', 'S'),
  ('married', 'M'),
  ('widowed', 'W');

INSERT INTO Canton(name, code) VALUES
('Aargau','AG'),
('Appenzell Innerrhoden','AI'),
('Appenzell Ausserrhoden','AR'),
('Bern','BE'),
('Basel-Landschaft','BL'),
('Basel-Stadt','BS'),
('Fribourg','FR'),
('Geneva','GE'),
('Glarus','GL'),
('Graubünden','GR'),
('Jura','JU'),
('Lucerne','LU'),
('Neuchâtel','NE'),
('Nidwalden','NW'),
('Obwalden','OW'),
('St. Gallen','SG'),
('Schaffhausen','SH'),
('Solothurn','SO'),
('Schwyz','SZ'),
('Thurgau','TG'),
('Ticino','TI'),
('Uri','UR'),
('Vaud','VD'),
('Valais','VS'),
('Zug','ZG'),
('Zurich','ZH');
