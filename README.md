# Getting Started

## Before starting

Make sure you have the following installed on your machine:

* Java IDE, e.g.: [Intellj IDEA](https://www.jetbrains.com/de-de/idea/download), [Eclipse](https://www.eclipse.org/downloads/)..
* Java Development Kit 14: [JDK14](https://jdk.java.net/14/)
* [download](https://maven.apache.org/download.cgi) and [install](https://maven.apache.org/install.html) maven 
* install [docker](https://docs.docker.com/get-docker/)

## About this test project

### Using maven

[Maven](https://maven.apache.org/what-is-maven.html) and gradle are both tools to describe the build process of Java project.
Many industry projects are using maven. When working in java projects as software developer many companies require that you know how to use maven.
If you are new to maven, have a look at this [tutorial](https://maven.apache.org/guides/getting-started/index.html). 

This project contains a java application that is build with maven.
The java project is a spring boot application. Spring is a java framework to create Java backend services. 

It may not surprise you that many industry project also use spring and spring boot, so if you are new to spring make sure to try a few basic [tutorials](https://spring.io/guides#getting-started-guides).

Now, after you got some basic understanding on maven and spring. Open the console and run:
```shell script
./mvnw clean install
``` 
in order to build the application.

The application is now in a stable state, but I left some TODOs inside the code for you. This way you can master your Java and unit testing skills.

### Using docker

Docker is a very popular standard when it comes to releasing your application.
While technically you could run the generated jar file on a cloud server directly, on production servers we want to run that app in an isolated context. For this we use docker. Here you find a useful guide to [learn the basics about docker](https://www.freecodecamp.org/news/docker-simplified-96639a35ff36/). 

To build our docker container run:
```shell script
docker build .
``` 
Then start it by calling:
```shell script
docker run container_name
``` 

### Continuous integration (CI) and Continuous Deployment (CD)

When working in a team you want to provide a stable version of your application to the customer.
Usually on git you have a stable main branch, the _master_. Each time you merge a pull request to the master branch, you want an automated pipeline to run the deployment for you.
All you need to do is to click on _merge_, convenient right?

On gitlab there is a special file _.gitlab-ci.yml_ that contains all build instructions what should be done after you pushed a new commit of your code. This project contains also a [gitlab-ci file](./.gitlab-ci.yml).

### About this tutorial

This is a very basic introduction into maven, spring, unit testing, docker and continuous integration. 

This project tries to teach you the basics on those topics but makes assumptions that you know some of those concepts. We did not cover, e.g. git, but it is important to understand git when working with CI/CD pipelines.

If you find some ideas difficult to understand, please write a comment or make a pull request to improve this tutorial.



